package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.models.Artist;

@Service
public class ArtistService {
@Autowired
    private AlbumService albumService;

    Artist minzy = new Artist(1,"Hoa Minzy");
    Artist katty = new Artist(2,"Katie perry");
    Artist blackpink = new Artist(4,"back pink");   

    public ArrayList<Artist> getAllArtists(){
        ArrayList<Artist> allArtist = new ArrayList<>();

        minzy.setAlbums(albumService.getAlbumList1());
        katty.setAlbums(albumService.getAlbumList2());
        blackpink.setAlbums(albumService.getAlbumList3());

        allArtist.add(minzy);
        allArtist.add(katty);
        allArtist.add(blackpink);

        return allArtist;
    }

    public Artist getIndexArtist(int index){
        Artist artist = null;
        ArrayList<Artist> allartist =getAllArtists();
        if (index >= 0 && index <= allartist.size())
        artist = allartist.get(index);
        return artist;

        }
    
}
