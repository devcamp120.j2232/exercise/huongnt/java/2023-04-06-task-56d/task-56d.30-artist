package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.models.Album;

@Service
public class AlbumService {

    public ArrayList<String> getSongList1(){
        ArrayList<String> songList1 = new ArrayList<String>();
        songList1.add("Huong");
        songList1.add("Hoa");
        return songList1;
    }

    public ArrayList<String> getSongList2(){
        ArrayList<String> songList1 = new ArrayList<String>();
        songList1.add("Chaha");
        songList1.add("Saranhe");
        return songList1;
    }



    public AlbumService albumService;
    Album vpop = new Album (1,"Vpop", getSongList1());
    Album kpop = new Album(2,"Kpop", getSongList2());
    Album rock = new Album(3,"Rock");
    Album pop = new Album(4,"pop");
    Album Jazz = new Album(5,"Jazz");
    Album Rap = new Album(6,"Rap");

    public ArrayList<Album> getAlbumList1(){
        ArrayList<Album> albumList1 = new ArrayList<>();
        albumList1.add(vpop);
        albumList1.add(kpop);
        return albumList1;
    }

    public ArrayList<Album> getAlbumList2(){
        ArrayList<Album> albumList2 = new ArrayList<>();
        albumList2.add(vpop);
        albumList2.add(kpop);
        return albumList2;
    }

    public ArrayList<Album> getAlbumList3(){
        ArrayList<Album> albumList3 = new ArrayList<>();
        albumList3.add(vpop);
        albumList3.add(kpop);
        return albumList3;
    }

    public ArrayList<Album> allAlbum(){
        ArrayList<Album> allAlbumList = new ArrayList<>();
        allAlbumList.add(vpop);
        allAlbumList.add(kpop);
        allAlbumList.add(rock);
        allAlbumList.add(pop);
        allAlbumList.add(Jazz);
        allAlbumList.add(Rap);
        return allAlbumList;

    }

}
